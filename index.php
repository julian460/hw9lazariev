<?php
session_start();
//Создаем условие чтобы всегда на старте отображался расчет в Гривне
if(!empty($_SESSION['selected'])){
    $selected = $_SESSION['selected'];
}
else{
    $selected = 'uah';
};
// Создаем масивы
$exchangeRate = [
    'uah'=> [
        'name' => 'Гривна',
        'course' => 1,],
    'usd'=>[
        'name' => 'Доллар',
        'course' => 27.1,],
    'eur'=> [
        'name' => 'Евро',
        'course' => 30.2,],
 ];
 $goods = [
    ['title' => 'Big TV' , 'price_val' => '5000' ],
    ['title' => 'Smartphone', 'price_val' =>'8679' ],
    ['title' => 'Laptop', 'price_val' => '42631'],
    ['title' => 'Headphones', 'price_val' => '999'],
    ['title' => 'Speaker', 'price_val' => '3168'],
    ['title' => 'Mouse', 'price_val' => '599'],
    ['title' => 'Computer', 'price_val' => '23156'],
    ['title' => 'Monitor', 'price_val' => '7500'],
    ['title' => 'Router', 'price_val' => '1233'],
    ['title' => 'Small TV', 'price_val' => '3333'],
    ]
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
    <meta name="description" content="My_shop">
    <title>My_shop</title>
</head>
<body>
<div class="conteiner" style="text-align: center">
    <h1>My shop!</h1>
</div>
<!-- Форма выбора валюты -->
<div class="container">
    <div class="row">
        <div class="col-3">
            <form action="/select.php" method="POST">
                <div class="form-control">
                    <div class="form-control">
                        <label>Выберите удобную валюту
                            <select name="selected" class="form-control">
                                <option selected value="uah">Гривна</option>
                                <option value="usd">Доллар</option>
                                <option value="eur">Евро</option>
                            </select>
                        </label>
                    </div>
                    <div class="form-control">        
                        <button class="btn btn-primary">Select</button>
                    </div>    
                </div>    
            </form>
        </div>
    </div>
</div>
<!-- Блок вывода товара-->
<div class="container">
    <div class="row">
                <!--Определяем ключи в массиве с валютами -->
                <?php foreach($exchangeRate as $key => $rate):?>
                    <!-- Сравниваем ключи с переменной полученой от нашей формы -->
                    <?php if($key == $selected):?>
                        <!-- Выкладываем весь товар подставив в расчет нужную валюту -->
                        <?php foreach($goods as $item):?>
                            <div class="col-6">
                                <div class="card">
                                    <div class="card-body">
                                        <h5><?=$item['title']?></h5>
                                        <p><?=$item['price_val']/$exchangeRate[$key]['course'] . ' ' . $exchangeRate[$key]['name']?></p>
                                    </div>    
                                </div>
                            </div>
                        <?php endforeach;?>
                    <?php endif;?>    
                <?php endforeach;?>
    </div>
</div>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>    
</body>
</html>